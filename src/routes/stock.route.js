import express, { Request, Response } from 'express';
import { getChartDataByCode } from '../controllers/stock.controller';
const router = express.Router();

router.route('/chart-data/:code').get(getChartDataByCode);

export { router as chartDataRouter };